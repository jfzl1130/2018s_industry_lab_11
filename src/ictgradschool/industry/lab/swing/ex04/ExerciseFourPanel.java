package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import static ictgradschool.industry.lab.swing.ex04.Direction.Down;
import static ictgradschool.industry.lab.swing.ex04.Direction.Up;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener{


    private  JButton moveButton;
    private Timer timer;
    private ArrayList<Balloon> balloons;


    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloons = new ArrayList<>();

        Balloon balloon = new Balloon(30,60);
        Balloon balloon2 = new Balloon(50,80);

        balloons.add(balloon);
        balloons.add(balloon2);

        this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);
        this.addKeyListener(this);

        this.timer = new Timer(100, this);


    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        for(int i=0;i<balloons.size();i++) {

            balloons.get(i).move();
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for(int i=0;i<balloons.size();i++) {

            balloons.get(i).draw(g);
        }
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        for(int i=0;i<balloons.size();i++) {

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                balloons.get(i).setDirection(Direction.Up);
                timer.start();

            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                balloons.get(i).setDirection(Direction.Down);
                timer.start();

            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                balloons.get(i).setDirection(Direction.Left);
                timer.start();

            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                balloons.get(i).setDirection(Direction.Right);
                timer.start();
            } else if (e.getKeyCode() == KeyEvent.VK_S) {
                timer.stop();
            }
        }
    }



    @Override
    public void keyReleased(KeyEvent e) {

    }
}