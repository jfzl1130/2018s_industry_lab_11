package ictgradschool.industry.lab.swing.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    private JButton button1, button2;
    private JTextField textField1,textField2,textField3,textField4;



    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        button1= new JButton("Calculate BMI");

        button2= new JButton("Calculate Healthy Weight");

        textField1= new JTextField(15);

        textField2= new JTextField(15);

        textField3= new JTextField(15);

        textField4= new JTextField(15);



        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.



        JLabel label = new JLabel("Height in metres:");
        JLabel labe2 = new JLabel("Weight in kilograms:");
        JLabel labe3 = new JLabel("your Body Mass index(BMI)is:");
        JLabel labe4 = new JLabel("Maximum Healthy Weight for your Height:");




        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)


        this.add(label);
        this.add(textField1);

        this.add(labe2);
        this.add(textField2);


        this.add(button1);
        this.add(labe3);
        this.add(textField3);

        this.add(button2);

        this.add(labe4);
        this.add(textField4);



        // TODO Add Action Listeners for the JButtons


        button1.addActionListener(this);
        button2.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        if (event.getSource() == button1) {
            double height= Double.parseDouble(textField1.getText());
            double weight= Double.parseDouble(textField2.getText());
            double BMI= weight/(height*height);
            roundTo2DecimalPlaces(BMI);
            textField3.setText("" + roundTo2DecimalPlaces(BMI));

        }
        else if (event.getSource() == button2) {
            double height= Double.parseDouble(textField1.getText());
            double MHW= roundTo2DecimalPlaces(24.9*height*height);
            textField4.setText("" + MHW);


        }




        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}