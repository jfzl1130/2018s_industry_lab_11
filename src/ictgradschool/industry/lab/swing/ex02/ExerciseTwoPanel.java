package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JButton buttonAdd, buttonSub;
    private JTextField textField1, textField2, textField3;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        buttonAdd = new JButton("Add");

        buttonSub = new JButton("Subtract");

        textField1 = new JTextField(10);

        textField2 = new JTextField(10);

        textField3 = new JTextField(15);

        JLabel label = new JLabel("Result:");

        this.add(textField1);
        this.add(textField2);

        this.add(buttonAdd);
        this.add(buttonSub);
        this.add(label);
        this.add(textField3);

        buttonAdd.addActionListener(this);
        buttonSub.addActionListener(this);
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == buttonAdd) {
            double number1 = Double.parseDouble(textField1.getText());
            double number2 = Double.parseDouble(textField2.getText());
            double add = roundTo2DecimalPlaces(number1 + number2);
            textField3.setText("" + roundTo2DecimalPlaces(add));

        } else if (e.getSource() == buttonSub) {
            double number1 = Double.parseDouble(textField1.getText());
            double number2 = Double.parseDouble(textField2.getText());
            double sub = roundTo2DecimalPlaces(number1 - number2);
            textField3.setText("" + roundTo2DecimalPlaces(sub));

        }
    }

}